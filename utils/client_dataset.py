# Taken from https://github.com/adap/flower/blob/85f80a95c0ecd3d22ed7eb51a53965b4b9242aff/baselines/flwr_baselines/publications/adaptive_federated_optimization/cifar/utils.py

from pathlib import Path
from typing import Tuple

from PIL import Image
from torch import Tensor, load
from torch.utils.data import Dataset
from torchvision.transforms import (
    Compose,
)


class ClientDataset(Dataset):
    """Client Dataset."""

    def __init__(self, path_to_data: Path, transform: Compose = None):
        """Implements local dataset.
        Args:
            path_to_data (Path): Path to local '.pt' file is located.
            transform (Compose, optional): Transforms to be used when sampling.
            Defaults to None.
        """
        super().__init__()
        self.transform = transform
        self.inputs, self.labels = load(path_to_data)

    def __len__(self) -> int:
        """Size of the dataset.
        Returns:
            int: Number of samples in the dataset.
        """
        return len(self.labels)

    def __getitem__(self, idx: int) -> Tuple[Tensor, int]:
        """Fetches item in dataset.
        Args:
            idx (int): Position of item being fetched.
        Returns:
            Tuple[Tensor, int]: Tensor image and respective label
        """
        this_input = Image.fromarray(self.inputs[idx])
        this_label = self.labels[idx]
        if self.transform:
            this_input = self.transform(this_input)

        return this_input, this_label