from tensorboardX import SummaryWriter


class TensorboardHandler():
    """
    Statefull class to hold the Tensorboard handler.

    :raises ValueError: non-init writer.
    :return: The tensorboard writer
    """
    writer = None
    identifier = None

    @classmethod
    def get_writer(cls, identifier=None, logdir="../runs"):
        """
        Get writer with given identifier.

        :param identifier: _description_, defaults to None
        :param logdir: _description_, defaults to "../runs"
        :raises ValueError: _description_
        :return: _description_
        """
        if (cls.writer is None) and (identifier is not None):
            cls.writer = SummaryWriter(logdir=logdir, comment=identifier)
            cls.identifier = identifier
        if (cls.writer is None) and (identifier is None):
            raise ValueError("Writer should be init with an identifier.")
        return cls.writer
