import torch
import torch.nn as nn
import numpy as np
import timeit
from io import BytesIO
from typing import cast

from utils.data_funcs import load_data, FEDERATED_DATASETS
from utils.model_funcs import initialise_model, \
                              train_model, \
                              evaluate_model, \
                              get_lr_scheduler
from utils.model_funcs import get_model_weights, \
                              set_model_weights, \
                              get_optimiser
from utils.logger import Logger
from utils.tensorboard_handler import TensorboardHandler


class ModelState(object):
    """
    Wrapper for loading state from spawned processes
    """
    def __init__(self, args):
        Logger.setup_logging(args.loglevel, logfile=args.logfile)
        Logger.get().info("Initializing model state")

        if torch.cuda.device_count():
            CUDA_SUPPORT = True
            torch.backends.cudnn.benchmark = True
        else:
            Logger.get().warning('CUDA unsupported!!')
            CUDA_SUPPORT = False
            args.gpu = "cpu"

        self.writer = TensorboardHandler.get_writer(logdir=args.tb_dir,
                                                    identifier=args.run_id)

        if args.deterministic:
            import torch.backends.cudnn as cudnn

            client_round_specific_seed = args.manual_seed + args.cid
            if hasattr(args, "epoch_global"):
                client_round_specific_seed += args.epoch_global
            np.random.seed(client_round_specific_seed)
            if CUDA_SUPPORT:
                cudnn.deterministic = args.deterministic
                cudnn.benchmark = not args.deterministic
                torch.cuda.manual_seed(client_round_specific_seed)
                torch.cuda.manual_seed_all(client_round_specific_seed)

        self.cid = args.cid
        self.nb_clients = args.nb_clients

        self.device = args.device

        # Dataset related
        self.trainset, self.testset = \
            load_data(args.data_path, args.dataset,
                      client_id=int(self.cid),
                      lda_concentration=args.lda_alpha,
                      load_trainset=True,
                      download=True)
        self.dataset_name = args.dataset

        # Model/optimiser related
        self.model = initialise_model(args.model, self.dataset_name)
        self.criterion = nn.CrossEntropyLoss()
        self.optimiser = get_optimiser(self.model.parameters(), args)
        self.current_epoch = 0
        self.epochs = args.epochs
        self.epoch_global = \
            args.epoch_global - 1 if hasattr(args, 'epoch_global') else 0
        self.total_rounds = \
            args.total_rounds if hasattr(args, 'total_rounds') else 0
        self.lr_type = args.lr_type
        self.scheduler = get_lr_scheduler(optimiser=self.optimiser,
                                          total_epochs=self.total_rounds,
                                          method=self.lr_type)
        for i in range(self.epoch_global):
            self.scheduler.step()

        self.batch_size = args.batch_size


def get_parameters(args, conn):
    state = ModelState(args)
    Logger.get().info(f"Client {state.cid}: get_parameters")
    state.writer.close()
    conn.send(get_model_weights(state.model))


def _bytes_to_ndarray(tensor: bytes) -> np.ndarray:
    """Deserialize NumPy array from bytes."""
    bytes_io = BytesIO(tensor)
    ndarray_deserialized = np.load(bytes_io, allow_pickle=False)
    return cast(np.ndarray, ndarray_deserialized)


def fit(args, ins_config, ins_tensors, conn):
    """
    Model training function.
    :param args: Arguments structure
    :param ins_config: Instructions configuration dictionary
    :param ins_tensors: Model parameters
    :param conn: Connection object
    """
    config = ins_config
    args.epoch_global = int(config['epoch_global'])
    args.total_rounds = int(config['total_rounds'])
    state = ModelState(args)

    Logger.get().info(
        f"Client {state.cid}: fit on global round {state.epoch_global}")

    weights = [_bytes_to_ndarray(tensor) for tensor in ins_tensors]
    fit_begin = timeit.default_timer()

    # Get training config
    # Here, we prioritise the CLI-args over the server-given configuration.
    # If we want the server given instructions to guide client training,
    #  simply don't define in CLI.
    epochs = int(state.epochs or config["epochs"])
    batch_size = int(state.batch_size or config["batch_size"])

    # Set model parameters
    set_model_weights(state.model, weights)
    state.model.to(state.device)

    # Get the data corresponding to this client
    dataset_size = len(state.trainset)
    nb_samples_per_clients = dataset_size // state.nb_clients
    dataset_indices = list(range(dataset_size))
    np.random.shuffle(dataset_indices)

    # Get starting and ending indices w.r.t cid
    if args.dataset not in FEDERATED_DATASETS:
        start_ind = int(state.cid) * nb_samples_per_clients
        end_ind = \
            (int(state.cid) * nb_samples_per_clients) + nb_samples_per_clients
        train_sampler = torch.utils.data.SubsetRandomSampler(
            dataset_indices[start_ind:end_ind]
        )

        Logger.get().info(f'Dataset indices: {start_ind} - {end_ind}')

        # Train model
        trainloader = torch.utils.data.DataLoader(
            state.trainset, batch_size=batch_size,
            shuffle=False, sampler=train_sampler
        )
    else:
        trainloader = torch.utils.data.DataLoader(
            state.trainset,
            batch_size=batch_size,
            num_workers=args.num_workers,
            shuffle=True
        )

    for _ in range(epochs):
        train_model(state.model,
                    trainloader,
                    state.criterion,
                    state.optimiser,
                    state.device,
                    state.current_epoch,
                    )
        state.scheduler.step()
        Logger.get().debug(f"LR is {state.scheduler.get_last_lr()}")
        state.current_epoch += 1

    state.writer.close()

    conn.send([
        get_model_weights(state.model),
        len(state.trainset),
        timeit.default_timer() - fit_begin,
    ])


def evaluate_m(args, ins_config, ins_parameters, conn):
    """
    Evaluate model function
    :param args: Argument structure
    :param ins_config: Instructions configuration dictionary
    :param ins_tensors: Model parameters
    :param conn: Connection object
    """
    state = ModelState(args)
    # Set the set so we are sure to generate the same batches
    # accross all clients.

    Logger.get().info(f"Client {state.cid}: evaluate")

    config = ins_config
    batch_size = int(state.batch_size or config["batch_size"])
    iters_lim = int(config["val_steps"])

    # weights = fl.common.parameters_to_weights(ins_parameters)
    weights = [_bytes_to_ndarray(tensor) for tensor in ins_parameters]

    # Use provided weights to update the local model
    set_model_weights(state.model, weights)

    # Evaluate the updated model on the local dataset
    if args.dataset not in FEDERATED_DATASETS:
        # Get the data corresponding to this client
        dataset_size = len(state.testset)
        nb_samples_per_clients = dataset_size // state.nb_clients
        dataset_indices = list(range(dataset_size))
        np.random.shuffle(dataset_indices)

        # Get starting and ending indices w.r.t cid
        start_ind = int(state.cid) * nb_samples_per_clients
        end_ind = \
            (int(state.cid) * nb_samples_per_clients) + nb_samples_per_clients
        test_sampler = torch.utils.data.SubsetRandomSampler(
            dataset_indices[start_ind:end_ind]
        )
        testloader = torch.utils.data.DataLoader(
            state.testset, batch_size=batch_size,
            shuffle=False, sampler=test_sampler)
    else:
        testloader = torch.utils.data.DataLoader(state.testset,
                                                 batch_size=batch_size,
                                                 num_workers=args.num_workers,
                                                 shuffle=False)

    metrics = evaluate_model(state.model, testloader, state.criterion,
                             state.device, state.current_epoch, iters_lim)
    loss = metrics['loss'].get_avg()
    metric = metrics['top_1'].get_avg()

    num_examples = len(state.testset)

    state.writer.close()
    conn.send([num_examples, loss, metric])
