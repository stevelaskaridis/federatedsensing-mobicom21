class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        """
        Update current meter with value and batch size

        :param val: The value
        :param n: The number of times this value is counted, defaults to 1
        """
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def get_avg(self):
        return self.avg

    def get_val(self):
        return self.val


def get_model_str_from_obj(model):
    """
    Get model string from model object

    :param model: The model object
    :return: The model string
    """
    return str(list(model.modules())[0]).split("\n")[0][:-1]
