import argparse
import os
import pickle
import zipfile
import numpy as np

from external.HAR import raw_data_processing
from external.HAR import data_pre_processing


def load_har_dataset(path, preprocess=False, download=True, client_id=None):
    """
    Load HAR dataset (and download if need be).

    :param path: Path to load from (and download to)
    :param preprocess: Whether to perform preprocessing, defaults to False
    :param download: Whether to download, defaults to True
    :param client_id: Client id to load, defaults to None
    :return: The partition for the specific client id.
    """
    if download:
        import requests
        dataset_url = \
            'https://github.com/mmalekzadeh/motion-sense/blob/master/data/B_Accelerometer_data.zip?raw=true'

        print("Downloading dataset")
        r = requests.get(dataset_url, allow_redirects=True)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(path + 'B_Accelerometer_data.zip', 'wb') as f:
            f.write(r.content)

        with zipfile.ZipFile(path + 'B_Accelerometer_data.zip', 'r') as zip_ref:
            zip_ref.extractall(path)

    if preprocess:
        accelerometer_data_folder_path = path + 'B_Accelerometer_data/'
        user_datasets = \
            raw_data_processing.process_motion_sense_accelerometer_files(
                accelerometer_data_folder_path)

        with open(path + 'motion_sense_user_split.pkl', 'wb') as f:
            pickle.dump({
                'user_split': user_datasets,
            }, f)

    # Parameters
    window_size = 400

    # Dataset Metadata
    dataset_name_user_split = 'motion_sense_user_split.pkl'

    label_list = ['null', 'sit', 'std', 'wlk', 'ups', 'dws', 'jog']

    label_map = dict([(l, i) for i, l in enumerate(label_list)])
    output_shape = len(label_list)

    # a fixed user-split
    test_users_fixed = [1, 14, 19, 23, 6]

    def get_fixed_split_users(har_users):
        # test_users = har_users[0::5]
        test_users = test_users_fixed
        train_users = [u for u in har_users if u not in test_users]
        return (train_users, test_users)

    with open(os.path.join(path, dataset_name_user_split), 'rb') as f:
        dataset_dict = pickle.load(f)
        user_datasets = dataset_dict['user_split']

    har_users = list(user_datasets.keys())
    train_users, test_users = get_fixed_split_users(har_users)
    print(f'Testing: {test_users}, Training: {train_users}')

    train_users = [(client_id)]
    print("User mode", train_users)

    np_train, np_val, _, norm_mean, norm_std = \
        data_pre_processing.pre_process_dataset_composite(
            user_datasets=user_datasets,
            label_map=label_map,
            output_shape=output_shape,
            train_users=train_users,
            test_users=test_users,
            window_size=window_size,
            shift=window_size//2,
            normalise_dataset=False,
            verbose=1)

    X_train = np_train[0]
    y_train = np_train[1]
    X_val = np_val[0]
    y_val = np_val[1]
    Norm_mean = norm_mean
    Norm_std = norm_std

    print(X_train.shape, y_train.shape,
          X_val.shape, y_val.shape,
          Norm_mean.shape, Norm_std.shape)

    print("Saving vectors...")
    client_id -= 1  # zero-based client IDs for dataloader.
    np.save(os.path.join(path, f'X_train_{client_id}.npy'), X_train)
    np.save(os.path.join(path, f'y_train_{client_id}.npy'), y_train)
    np.save(os.path.join(path, f'X_val_{client_id}.npy'), X_val)
    np.save(os.path.join(path, f'y_val_{client_id}.npy'), y_val)
    np.save(os.path.join(path, f'Norm_mean_{client_id}.npy'), Norm_mean)
    np.save(os.path.join(path, f'Norm_std_{client_id}.npy'), Norm_std)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='FL data selection.')
    parser.add_argument("-o", "--output", default='../data/har/')
    args = parser.parse_args()

    for cid in range(1, 25):
        print(f"Creating dataset for cid={cid}")
        load_har_dataset('../data/har/', cid == 1, cid == 1, cid)
