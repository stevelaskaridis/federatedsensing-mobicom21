# Adapted from https://github.com/adap/flower/blob/85f80a95c0ecd3d22ed7eb51a53965b4b9242aff/baselines/flwr_baselines/publications/adaptive_federated_optimization/cifar/utils.py

import argparse
import os
from pathlib import Path
from typing import List, Optional
import numpy as np
import numpy.typing as npt
import torch
from sklearn.model_selection import train_test_split

from flwr.dataset.utils.common import (
    XY,
    create_lda_partitions,
)
from torchvision.datasets import CIFAR10


def save_partitions(
    list_partitions: List[XY], fed_dir: Path, partition_type: str = "train"
):
    """Saves partitions to individual files.
    Args:
        list_partitions (List[XY]): List of partitions to be saves
        fed_dir (Path): Root directory where to save partitions.
        partition_type (str, optional): Partition type ("train" or "test"). Defaults to "train".
    """
    for idx, partition in enumerate(list_partitions):
        path_dir = os.path.join(fed_dir, f"{idx}")
        if not os.path.exists(path_dir):
            os.makedirs(path_dir)
        torch.save(partition, os.path.join(path_dir, f"{partition_type}.pt"))


def partition_cifar10_and_save(
    dataset: XY,
    fed_dir: Path,
    dirichlet_dist: Optional[npt.NDArray[np.float32]] = None,
    num_partitions: int = 500,
    concentration: float = 0.1,
    val_ratio: float = 0.2,
) -> np.ndarray:
    """Creates and saves partitions for CIFAR10.
    Args:
        dataset (XY): Original complete dataset.
        fed_dir (Path): Root directory where to save partitions.
        dirichlet_dist (Optional[npt.NDArray[np.float32]], optional):
            Pre-defined distributions to be used for sampling if exist. Defaults to None.
        num_partitions (int, optional): Number of partitions. Defaults to 500.
        concentration (float, optional): Alpha value for Dirichlet. Defaults to 0.1.
        val_ratio (float): Proportion of validation set. Defaults to 0.2.
    Returns:
        np.ndarray: Generated dirichlet distributions.
    """
    # Create partitions
    clients_partitions, dist = create_lda_partitions(
        dataset=dataset,
        dirichlet_dist=dirichlet_dist,
        num_partitions=num_partitions,
        concentration=concentration,
    )

    # Split to train and validation
    train_partitions = [[0, 0] for _ in range(len(clients_partitions))]
    test_partitions = [[0, 0] for _ in range(len(clients_partitions))]
    for p in range(len(clients_partitions)):
        X = clients_partitions[p][0]
        y = clients_partitions[p][1]

        if val_ratio > 0:
            X_train, X_test, y_train, y_test = \
                train_test_split(X, y, test_size=val_ratio,)
            train_partitions[p][0] = X_train
            train_partitions[p][1] = y_train
            test_partitions[p][0] = X_test
            test_partitions[p][1] = y_test

    # Save partions
    if val_ratio > 0:
        save_partitions(
            list_partitions=train_partitions,
            fed_dir=fed_dir,
            partition_type="train")
        save_partitions(
            list_partitions=test_partitions,
            fed_dir=fed_dir,
            partition_type="test")
    else:
        save_partitions(list_partitions=clients_partitions, fed_dir=fed_dir)

    return dist


def gen_cifar10_partitions(
    path_original_dataset: Path,
    num_total_clients: int,
    lda_concentration: float,
    val_ratio: float,
) -> Path:
    """Defines root path for partitions and calls functions to create them.
    Args:
        path_original_dataset (Path): Path to original (unpartitioned) dataset.
        num_total_clients (int): Number of clients.
        lda_concentration (float): Concentration (alpha) used when generation Dirichlet
        distributions.
        val_ratio (float): Proportion of validation set.
    Returns:
        Path: [description]
    """
    fed_dir = os.path.join(
        path_original_dataset,
        "partitions",
        f"{num_total_clients}",
        f"{lda_concentration:.2f}"
    )

    trainset = CIFAR10(root=path_original_dataset, train=True, download=True)
    flwr_trainset = (trainset.data, np.array(trainset.targets, dtype=np.int32))
    partition_cifar10_and_save(
        dataset=flwr_trainset,
        fed_dir=fed_dir,
        dirichlet_dist=None,
        num_partitions=num_total_clients,
        concentration=lda_concentration,
        val_ratio=val_ratio,
    )

    return fed_dir


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='FL data selection.')
    parser.add_argument("-o", "--output", default='../data/cifar10/')
    parser.add_argument("-n", "--num-clients", default=100)
    parser.add_argument("-l", "--lda-concentration", default=0.1)
    parser.add_argument("-t", "--test-ratio", default=0.2)
    args = parser.parse_args()

    gen_cifar10_partitions(
        args.output, args.num_clients, args.lda_concentration,
        args.test_ratio
    )
