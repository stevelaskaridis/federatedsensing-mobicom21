from collections import OrderedDict
import numpy as np
import time
import torch
from torch.optim.lr_scheduler import MultiStepLR, CosineAnnealingLR

import models
from utils.logger import Logger
from utils.checkpointing import load_checkpoint
from utils.data_funcs import get_num_classes
from utils.utils import AverageMeter, get_model_str_from_obj
from utils.tensorboard_handler import TensorboardHandler


def initialise_model(model_name, dataset,
                     resume_from=None, load_best=None):
    """
    Initialise the model with (potentially) pretrained weights.

    :param model_name: The name of the model to initialise
    :param use_pretrained: Whether to use a pretrained model or not
    :param dataset: The dataset we are training with (used for final linear layer)
    :pram resume_from: Path to load custom weights from
    :para load_best: Whether to load best or last model from given dir
    :return: the initialised model
    """
    model = getattr(models, model_name)(num_classes=get_num_classes(dataset))

    if resume_from:
        model = load_checkpoint(resume_from, load_best)

    return model


def train_model(model,
                train_loader,
                criterion,
                optimiser,
                device,
                epoch,
                print_freq=10,
                persist_tensorboard=True):
    """
    Train the model for one epoch.

    :param model: The model to train
    :param train_loader: The trainset data loader
    :param criterion: The criterion to use for training.
    :param optimiser: The optimiser to use for training.
    :param device: Where to train on (e.g. cuda:0, cpu, etc.)
    :param epoch: The current epoch number
    :param print_freq: How frequently to print training progress. Defaults to 10.
    :param persist_tensorboard: Whether to persist information in Tensorboard. Defaults to True.
    """
    top1_acc_meter = AverageMeter()
    top5_acc_meter = AverageMeter()
    loss_meter = AverageMeter()
    if persist_tensorboard:
        writer = TensorboardHandler.get_writer()
    model.train()
    hidden = None

    for i, (x, y) in enumerate(train_loader):
        start_ts = time.time()
        batch_size = x.shape[0]
        x = x.to(device)
        y = y.to(device)
        dataload_duration = time.time() - start_ts
        is_rnn = get_model_str_from_obj(model) in models.RNN_MODELS
        x, y = get_inputs(x, y, model, x.shape[0], device, is_rnn, hidden=None)
        inference_duration = 0.
        backprop_duration = 0.

        optimiser.zero_grad()

        start_ts = time.time()
        output = model(*x)
        if is_rnn:
            output, hidden = output
            y = torch.max(y, 1)[1]

        single_inference = time.time() - start_ts
        inference_duration += single_inference

        loss = compute_loss(model, criterion, output, y)
        loss.backward()
        backprop_duration += time.time() - (start_ts + single_inference)

        loss_meter.update(loss.item(), batch_size)
        acc = compute_accuracy(output, y, (1, 5))
        top1_acc_meter.update(acc[0], batch_size)
        top5_acc_meter.update(acc[1], batch_size)

        optimiser.step()
        optimiser.zero_grad()

        if i % print_freq == 0:
            Logger.get().info("Epoch [{epoch}][{current_batch}/{total_batches}]\t"
                              "DataLoad time {dataload_duration:.3f}\t"
                              "F/W time {inference_duration:.3f}\t"
                              "B/W time {backprop_duration:.3f}\t"
                              "Loss {loss:.4f}\t"
                              "Prec@1 {prec1:.3f}\t"
                              "Prec@5 {prec5:.3f}\t".format(
                                  epoch=epoch,
                                  current_batch=i,
                                  total_batches=len(train_loader),
                                  dataload_duration=dataload_duration,
                                  inference_duration=inference_duration,
                                  backprop_duration=backprop_duration,
                                  loss=loss_meter.get_val(),
                                  prec1=top1_acc_meter.get_val(),
                                  prec5=top5_acc_meter.get_val()))

    if persist_tensorboard:
        writer.add_scalars('{model}_{run_id}_accuracy'.format(
                                        run_id=TensorboardHandler.identifier,
                                        model=get_model_str_from_obj(model)),
                           {'train_prec1': top1_acc_meter.get_avg(),
                            'train_prec5': top5_acc_meter.get_avg()}, epoch)
        writer.add_scalars('{model}_{run_id}_loss'.format(
                                        run_id=TensorboardHandler.identifier,
                                        model=get_model_str_from_obj(model)),
                           {'train_loss': loss_meter.get_avg()}, epoch)


def evaluate_model(model,
                   val_loader,
                   criterion,
                   device,
                   epoch,
                   iters_limit=None,
                   print_freq=10,
                   persist_tensorboard=True):
    """
    Perform an evaluation od the model on the given data.

    :param model: The model to evaluate.
    :param val_loader: The dataloader for the evaluation.
    :param criterion: The criterion to use for training.
    :param device: Where to train on (e.g. cuda:0, cpu, etc.)
    :param epoch: The current epoch number
    :param iters_limit: Limit number of iterations (for evaluation on clients), defaults to None
    :param print_freq: How frequently to print training progress. Defaults to 10.
    :param persist_tensorboard: Whether to persist information in Tensorboard. Defaults to True.
    :return: Dictionary containing evaluation metrics.
    """
    top1_acc_meter = AverageMeter()
    top5_acc_meter = AverageMeter()
    loss_meter = AverageMeter()

    if persist_tensorboard:
        writer = TensorboardHandler.get_writer()

    model.eval()
    hidden = None

    with torch.no_grad():
        for i, (x, y) in enumerate(val_loader):
            if (iters_limit is not None) and (i >= iters_limit):
                break
            start_ts = time.time()
            x = x.to(device)
            y = y.to(device)
            dataload_duration = time.time() - start_ts
            batch_size = x.shape[0]
            is_rnn = get_model_str_from_obj(model) in models.RNN_MODELS
            x, y = get_inputs(x, y, model, batch_size,
                              device, is_rnn, hidden=None)

            out = model(*x)
            inference_duration = time.time() - (start_ts + dataload_duration)
            if is_rnn:
                out, hidden = out
                y = torch.max(y, 1)[1]

            loss = compute_loss(model, criterion, out, y)
            loss_meter.update(loss.item(), batch_size)

            accs = compute_accuracy(out, y, (1, 5))
            top1_acc_meter.update(accs[0], batch_size)
            top5_acc_meter.update(accs[1], batch_size)

            if i % print_freq == 0:
                Logger.get().info(
                    "Test[{current_batch}/{total_batches}]\t"
                    "DataLoad time {dataload_duration:.3f}\t"
                    "F/W time {inference_duration:.3f}\t"
                    "Loss {loss:.4f}\t"
                    "Prec@1 {prec1:.3f}\t"
                    "Prec@5 {prec5:.3f}\t".format(
                        current_batch=i,
                        total_batches=len(val_loader),
                        dataload_duration=dataload_duration,
                        inference_duration=inference_duration,
                        loss=loss_meter.get_avg(),  # avg of avgs
                        prec1=top1_acc_meter.get_avg(),  # avg of vals
                        prec5=top5_acc_meter.get_avg(),
                    ))

        if persist_tensorboard:
            writer.add_scalars('{model}_{run_id}_accuracy'.format(
                                    run_id=TensorboardHandler.identifier,
                                    model=get_model_str_from_obj(model),),
                               {'val_prec1': top1_acc_meter.get_avg(),
                                'val_prec5': top5_acc_meter.get_avg()}, epoch)
            writer.add_scalars('{model}_{run_id}_loss'.format(
                                    run_id=TensorboardHandler.identifier,
                                    model=get_model_str_from_obj(model),),
                               {'val_loss': loss_meter.get_avg()}, epoch)

        metrics = {
            'loss': loss_meter,
            'top_1': top1_acc_meter,
            'top_5': top5_acc_meter,
        }

        return metrics


def compute_loss(model, criterion, output, y):
    """
    Compute the loss given model, criterion and data.

    :param model: The model
    :param criterion: The criterion
    :param output: The output of the model
    :param y: The samples labels
    :return: The loss
    """
    if type(output) == list:  # and len(output) > 1:
        if type(model).__name__.lower() == 'inception3':
            loss = criterion(output[0], y) + 0.4 * criterion(output[1], y)
        else:
            loss = sum([criterion(out, y) for out in output])
    else:
        loss = criterion(output, y)
    return loss


def compute_accuracy(output, label, topk=(1,)):
    """
    Extract the accuracy of the model.

    :param output: The output of the model
    :param label: The correct target label
    :param topk: Which accuracies to return (e.g. top1, top5)
    :return: The accuracies requested
    """
    maxk = max(topk)
    batch_size = label.size(0)

    if len(output.size()) == 1:
        _, pred = output.topk(maxk, 0, True, True)
    else:
        _, pred = output.topk(maxk, 1, True, True)
    if pred.size(0) != 1:
        pred = pred.t()

    if pred.size() == (1,):
        correct = pred.eq(label)
    else:
        correct = pred.eq(label.view(1, -1).expand_as(pred))
    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size).item())
    return res


def get_optimiser(params_to_update, args):
    """
    Get the optimiser requested.

    :param params_to_update: Model parameters to register optimiser on.
    :param args: Args struct
    :raises ValueError: If optimiser is not supported
    :return: The initialised optimiser
    """
    if args.optimiser == 'sgd':
        optimiser = torch.optim.SGD(params_to_update, args.lr,
                                    momentum=args.momentum,
                                    weight_decay=args.weight_decay,
                                    nesterov=args.nesterov)
    elif args.optimiser == 'adam':
        optimiser = torch.optim.Adam(params_to_update, args.lr)
    elif args.optimiser == 'rmsprop':
        optimiser = torch.optim.RMSprop(params_to_update, args.lr,
                                        momentum=args.momentum,
                                        weight_decay=args.weight_decay)
    else:
        raise ValueError("optimiser not supported")

    return optimiser


def get_lr_scheduler(optimiser, total_epochs, method='static'):
    """
    Implement learning rate scheduler.

    :param optimiser: A reference to the optimiser being usd
    :param total_epochs: The total number of epochs (from the args)
    :param method: The strategy to adjust the learning rate
        (multistep, cosine or static)
    :return: scheduler on current step/epoch/policy
    """
    if method == 'cosine':
        # T_total = total_epochs * nBatch
        # T_cur = (epoch % total_epochs) * nBatch + batch
        # lr = 0.5 * initial_lr * (1 + math.cos(math.pi * T_cur / T_total))
        return CosineAnnealingLR(optimiser, total_epochs)
    elif method == 'static':
        return MultiStepLR(optimiser, [total_epochs + 1])
    if method == 'cifar_1':  # VGGs + ResNets
        return MultiStepLR(optimiser,
                           [int(0.5 * total_epochs),
                            int(0.75 * total_epochs)],
                           gamma=0.1)
    if method == 'cifar_2':  # WideResNets
        return MultiStepLR(optimiser,
                           [int(0.3 * total_epochs),
                            int(0.6 * total_epochs),
                            int(0.8 * total_epochs)],
                           gamma=0.2)

    raise ValueError(f"{method} is not defined as scheduler name.")


def model_to_device(model, device):
    if type(device) == list:  # if to allocate on more than one device
        model = model.to(device[0])
    else:
        model.to(device)
    return model


def weights_to_state_dict(model, weights):
    return OrderedDict(
        {
            k: torch.Tensor(np.atleast_1d(v))
            for k, v in zip(model.state_dict().keys(), weights)
        }
    )


def set_model_weights(model, weights, strict=True):
    """
    Sets the weight models in-place.
    To be used to integrate new updated weights.

    :param model: The model to be updated
    :param weights: (fl.common.Weights) ndarrays list representing weights
    :param strict: To require 1-to-1 parameter to weights associations
    """
    state_dict = weights_to_state_dict(model, weights)
    model.load_state_dict(state_dict, strict=strict)


def get_weights_from_state_dict(state_dict):
    """
    Get weights from state dict as list.

    :param state_dict: The model's state dict
    :return: weights as a list
    """
    return [val.cpu().numpy() for _, val in state_dict.items()]


def get_model_weights(model):
    """
    Get model weights as a list of NumPy ndarrays.

    :param model: the model
    :return: weights of the model as list.
    """

    return get_weights_from_state_dict(model.state_dict())


def get_inputs(x, y, model, batch_size, device, is_rnn, hidden=None):
    """
    Get proper inputs of the model.

    :param x: The input sample
    :param y: The label
    :param model: The model
    :param batch_size: The batch size
    :param device: The device the model/data reside on
    :param is_rnn: Whether the model is an RNN or not.
    :param hidden: Whether to initialise hidden layer or not, defaults to None
    :return: The input, label tuple.
    """
    if not is_rnn:
        input = (x,)
    else:
        hidden = model.init_hidden(batch_size, device) \
            if hidden is None else hidden
        input = (x, hidden)

    return input, y
