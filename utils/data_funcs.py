import torch
import torchvision
from torchvision import transforms
import numpy as np
import os
from torch.utils.data import TensorDataset

from .client_dataset import ClientDataset


FEDERATED_DATASETS = ["cifar10", "har"]


def get_num_classes(dataset):
    """
    Get number of classification classes given the dataset name
    """
    if dataset.startswith("cifar"):
        return int(dataset[len("cifar"):])
    elif dataset == 'har':
        return 7
    else:
        raise ValueError(f"Dataset {dataset} unknown.")


def get_num_clients(dataset):
    """
    Get number of clients given the dataset name
    """
    if dataset.startswith("cifar"):
        return 100
    elif dataset == 'har':
        return 24
    else:
        raise ValueError(f"Dataset {dataset} unknown.")


def load_data(path, dataset, load_trainset=True,
              download=True, client_id=None,
              lda_concentration=None):
    """
    Load data from a given path and apply transformations.
    :param path: Path to load the dataset from
    :param dataset: Name of the dataset to load
    :param load_trainset: Whether to load trainset or not
    :param download: Whether to download from the WWW
    :param client_id: Client id to return only that part of the dataset
    :return: tuple of trainset, testset
    """
    if (client_id is not None) and (client_id < 0):
        client_id = None  # if client_id is None, then load the whole dataset

    dataset = dataset.lower()
    path = os.path.join(path, dataset)
    if dataset.startswith("cifar"):  # CIFAR-10/100

        torch_version = int(torch.__version__.split('+')[0].replace('.', ''))
        if torch_version < 160:  # version 1.5's flip works with PIL images
            transform_train = transforms.Compose([
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465),
                                     (0.2023, 0.1994, 0.2010)),
            ])
        else:  # later versions work with tensors
            transform_train = transforms.Compose([
                transforms.RandomCrop(32, padding=4),
                transforms.ToTensor(),
                transforms.RandomHorizontalFlip(),
                transforms.Normalize((0.4914, 0.4822, 0.4465),
                                     (0.2023, 0.1994, 0.2010)),
            ])

        transform_test = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465),
                                 (0.2023, 0.1994, 0.2010)),
        ])

        if dataset == "cifar10":
            if load_trainset:
                if client_id is None:
                    trainset = torchvision.datasets.CIFAR10(
                        root=path, train=True,
                        download=download, transform=transform_train)
                else:
                    trainset = ClientDataset(os.path.join(path,
                                                          "partitions",
                                                          f"{get_num_clients(dataset)}",
                                                          f"{lda_concentration:.2f}",
                                                          f"{client_id}",
                                                          "train.pt"),
                                             transform=transform_train)
            if client_id is None:
                testset = torchvision.datasets.CIFAR10(
                    root=path, train=False,
                    download=download, transform=transform_test)
            else:
                testset = ClientDataset(os.path.join(path,
                                                     "partitions",
                                                     f"{get_num_clients(dataset)}",
                                                     f"{lda_concentration:.2f}",
                                                     f"{client_id}",
                                                     "test.pt"),
                                        transform=transform_test)

    elif dataset == 'har':
        trainset, testset, _, _ = \
            load_har_dataset(path, client_id=client_id,
                             preprocess=True, global_stats=True)
    else:
        raise NotImplementedError(f'{dataset} is not implemented.')

    if not load_trainset:
        trainset = None

    return trainset, testset


def load_har_dataset(path, preprocess=True, client_id=None, global_stats=True):
    """
    Load HAR dataset.
    :param path: Path to load the data from
    :param preprocess: Normalise the data
    :param client_id: Client id to load data partition
    :param global_stats: Whether to use global stats or not
    :return: trainset, tesetset and global stats
    """
    if client_id is None:
        x_train = []
        y_train = []
        x_val = []
        y_val = []
        for client_id in range(24):
            if preprocess and (not global_stats):
                norm_mean = np.load(os.path.join(path,
                                                 f"Norm_mean_{client_id}.npy"))
                norm_std = np.load(os.path.join(path,
                                                f"Norm_std_{client_id}.npy"))
            else:
                norm_mean = 0
                norm_std = 1
            x_train.append(
                (np.load(os.path.join(path, f"X_train_{client_id}.npy"))
                    - norm_mean)/norm_std)
            y_train.append(
                np.load(os.path.join(path, f"y_train_{client_id}.npy")))
            x_val.append(
                (np.load(os.path.join(path, f"X_val_{client_id}.npy"))
                    - norm_mean)/norm_std)
            y_val.append(
                np.load(os.path.join(path, f"y_val_{client_id}.npy")))
        x_train = np.vstack(x_train)
        y_train = np.vstack(y_train)
        x_val = np.vstack(x_val)
        y_val = np.vstack(y_val)
        if preprocess and global_stats:
            norm_mean = x_train.mean(axis=(0, 1))
            norm_std = x_train.std(axis=(0, 1))

        trainset, testset = \
            har_to_datasets(x_train, y_train,
                            x_val, y_val,
                            norm_mean, norm_std,
                            preprocess=(preprocess and global_stats))
    else:
        x_train = np.load(os.path.join(path, f"X_train_{client_id}.npy"))
        y_train = np.load(os.path.join(path, f"y_train_{client_id}.npy"))
        x_val = np.load(os.path.join(path, f"X_val_{client_id}.npy"))
        y_val = np.load(os.path.join(path, f"y_val_{client_id}.npy"))
        norm_mean = np.load(os.path.join(path, f"Norm_mean_{client_id}.npy"))
        norm_std = np.load(os.path.join(path, f"Norm_std_{client_id}.npy"))
    trainset, testset = har_to_datasets(x_train, y_train,
                                        x_val, y_val,
                                        norm_mean, norm_std,
                                        preprocess=True)

    return trainset, testset, norm_mean, norm_std


def har_to_datasets(X_train, y_train, X_val, y_val,
                    norm_mean, norm_std, preprocess=True):
    """
    Returns MotionSense HAR dataset.
    :param X_train: Train features
    :param y_train: Train label
    :param X_val: Validation features
    :param y_val: Validation label
    :param norm_mean: mean to normalise with
    :param norm_std: std to standardise with
    :param preproces: Whether to normalise data or not
    :return: Tensor dataset for train and validation
    """
    if preprocess:
        X_train = (X_train - norm_mean)/norm_std
        X_val = (X_val - norm_mean)/norm_std

    X_train, X_val = \
        [torch.tensor(arr, dtype=torch.float32) for arr in (X_train, X_val,)]
    y_train, y_val = \
        [torch.tensor(arr, dtype=torch.float32) for arr in (y_train, y_val)]

    train_ds = TensorDataset(X_train, y_train)

    valid_ds = TensorDataset(X_val, y_val)

    return train_ds, valid_ds
