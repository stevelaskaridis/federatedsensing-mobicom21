PYTHON=${PYTHON:-"python"}
DATASET=${DATASET:-har}
MODEL=${MODEL:-har}
CLIENTS=${CLIENTS:-24}
MIN_SAMPLE=${MIN_SAMPLE:-10}
SAMPLE_FRACTION=${SAMPLE_FRACTION:-0.5}
ROUNDS=${ROUNDS:-10}
TAG=${TAG:-"SERVER"}
GPU=${GPU:-0}

echo "Launching server with $CLIENTS clients and sampling $MIN_SAMPLE"

$PYTHON ../server.py \
    --dataset $DATASET \
    --model $MODEL \
    --min-num-clients $CLIENTS \
    --gpu $GPU \
    -b 128 \
    -n 1 \
    --min-sample-size $MIN_SAMPLE \
    --sample-fraction $SAMPLE_FRACTION \
    --rounds $ROUNDS \
    --logfile ../logs/$TAG \
    --run-id $TAG \
    --deterministic \
    $@ # > /dev/null 2>&1 &
