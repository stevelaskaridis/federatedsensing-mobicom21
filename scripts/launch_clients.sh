#!/bin/bash

PYTHON=${PYTHON:-python}
DATASET=${DATASET:-cifar10}
MODEL=${MODEL:-resnet20}
BATCH_SIZE=${BATCH_SIZE:-4}
EPOCHS=${EPOCHS:-1}  # This is local epochs on the client
CLIENTS=${CLIENTS:-10}
CLIENTS_PER_GPU=$CLIENTS
LOCAL_GPUS=${LOCAL_GPUS:-1}
TAG=${TAG:-"CLIENT"}
CID_START=${CID_START:-0}
NUM_CLIENTS=${NUM_CLIENTS:-$CLIENTS}

for ((i=0;i<$CLIENTS;++i)); do

    capped_i=$(( i % (CLIENTS_PER_GPU * $LOCAL_GPUS ) ))
    gpu=$(( ($capped_i / $CLIENTS_PER_GPU) ))
    if (( (i % (LOCAL_GPUS * CLIENTS_PER_GPU) == 0) && (i != 0) )); then
        echo "Sleeping"
        wait < <(jobs -p)
    fi

    cid=$(( i + CID_START ))

    echo "Launching client $cid"
    $PYTHON ../client.py \
        --dataset $DATASET \
        --cid $cid \
        --run-id $TAG-$cid \
        --model $MODEL \
        -b $BATCH_SIZE \
        -n 1 \
        --nb-clients $NUM_CLIENTS \
        --gpu $gpu \
        --epochs $EPOCHS \
        --logfile ../logs/$TAG-$cid \
        --deterministic \
        $@ &
done
