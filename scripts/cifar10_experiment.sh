#!/bin/bash

SAMPLE_FRACTION=0.1 ROUNDS=500 DATASET="cifar10" MODEL="resnet20" CLIENTS=100 ./launch_server.sh --eval-every 1 &
sleep 5
DATASET=cifar10 MODEL=resnet20 CLIENTS=100 BATCH_SIZE=32 LR=0.01 ./launch_clients.sh  --lr-type cifar_1