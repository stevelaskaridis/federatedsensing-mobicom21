import flwr as fl
from functools import partial
import sys
import torch
import torch.nn as nn

from opts import parse_server_args
from utils.model_funcs import initialise_model, \
                              evaluate_model, \
                              set_model_weights, \
                              get_model_weights
from utils.utils import get_model_str_from_obj
from utils.data_funcs import load_data
from utils.checkpointing import save_checkpoint
from utils.logger import Logger
from utils.tensorboard_handler import TensorboardHandler


def get_eval_config_fn(args):
    """
    Function wrapper returning the evaluate config.
    """

    def evaluate_config(rnd: int):
        val_steps = 5 if rnd < 4 else 10
        return {
            "val_steps": val_steps,
            "batch_size": args.batch_size,
        }

    return evaluate_config


def get_eval_fn(
        model,
        testset,
        criterion,
        args):
    """
    Return an evaluation function for centralized evaluation.

    :param model: The global model
    :param testset: The testset to evaluate on
    :param criterion: The criterion (e.g. Cross-entropy)
    :param args: The arguments to get parameters from
    :return: The evaluation function
    """

    curr_round = 0
    previous_loss = 0
    best_acc = 0
    previous_avg_metric = None
    device = args.device
    batch_size = args.batch_size
    num_workers = args.num_workers
    eval_every = args.eval_every
    rounds = args.rounds
    model_name = get_model_str_from_obj(model)

    def evaluate(weights):
        """Function definition for server-side evaluation."""

        nonlocal curr_round
        curr_round += 1
        nonlocal best_acc
        nonlocal previous_avg_metric

        set_model_weights(model, weights)

        if ((curr_round-1) % eval_every == 0) or (curr_round == rounds):
            model.to(device)
            testloader = torch.utils.data.DataLoader(testset,
                                                     batch_size=batch_size,
                                                     num_workers=num_workers,
                                                     shuffle=False)
            metrics = evaluate_model(model, testloader, criterion,
                                     device, curr_round)
            metrics['epoch'] = curr_round
            loss = metrics['loss'].get_avg()
            avg_metric = metrics['top_1'].get_avg()

            # Save model checkpoint
            model_filename = \
                 f'{model_name}_{args.run_id}_checkpoint_{curr_round:0>2d}.pth.tar'
            is_best = avg_metric > best_acc
            save_checkpoint(model,
                            model_filename,
                            is_best=is_best,
                            args=args,
                            metrics=metrics)
            previous_avg_metric = avg_metric
        else:
            Logger.get().warning(
                f"Skipping evalaution on round {curr_round}. Wait for "
                f"{args.eval_every - ((curr_round-1) % args.eval_every)} "
                "or last.")
            loss = previous_loss
            avg_metric = previous_avg_metric

        return loss, {"accuracy": avg_metric}

    return evaluate


def get_fit_config_fn(epochs, batch_size, total_rounds):
    """
    Return a configuration function to define the next round's runtime.

    :param epochs: The number of epochs to train on (locally)
    :param batch_size: The batch size to use in training
    :param total_rounds: The total rounds to train on.
    """
    def fit_config(rnd):
        """Return a configuration with static batch size and (local) epochs."""
        config = {
            "epoch_global": str(rnd),
            "epochs": str(epochs),
            "batch_size": str(batch_size),
            "total_rounds": str(total_rounds),
        }
        return config

    return fit_config


def main(args):
    args.device = args.gpu[0] if type(args.gpu) == list else args.gpu
    args.lr = None

    # For tensorboard
    TensorboardHandler.get_writer(logdir=args.tb_dir, identifier=args.run_id)

    # Parse determinism
    if args.deterministic:
        import torch.backends.cudnn as cudnn

        if CUDA_SUPPORT:
            cudnn.deterministic = args.deterministic
            cudnn.benchmark = not args.deterministic
            torch.cuda.manual_seed(args.manual_seed)
            torch.cuda.manual_seed_all(args.manual_seed)

    # Load validation set
    _, testset = load_data(args.data_path, args.dataset,
                           load_trainset=False, download=True)

    # Define the model
    model = initialise_model(args.model,
                             dataset=args.dataset,
                             resume_from=args.resume_from,
                             load_best=True)

    pretrained_params = \
        fl.common.weights_to_parameters(
            get_model_weights(model)) if args.resume_from else None

    criterion = nn.CrossEntropyLoss()

    # Setup FL job description
    client_mgr = fl.server.SimpleClientManager()  # Client org & sampling
    partial_strategy = partial(fl.server.strategy.FedAvg,
                               fraction_fit=args.sample_fraction,
                               min_fit_clients=args.min_sample_size,
                               min_available_clients=args.min_num_clients,
                               on_fit_config_fn=get_fit_config_fn(
                                                   args.epochs,
                                                   args.batch_size,
                                                   args.rounds),
                               initial_parameters=pretrained_params)
    if args.client_eval:
        strategy = partial_strategy(
            on_evaluate_config_fn=get_eval_config_fn(args),
            fraction_eval=args.sample_fraction,
            min_eval_clients=args.min_num_clients,
        )
    else:
        strategy = partial_strategy(
            eval_fn=get_eval_fn(model,
                                testset,
                                criterion,
                                args,),  # server-side evaluation function
        )
    server = fl.server.Server(client_manager=client_mgr, strategy=strategy)

    # Run server
    fl.server.start_server(
        args.server_address,
        server,
        config={"num_rounds": args.rounds},
    )


if __name__ == "__main__":
    global CUDA_SUPPORT
    if torch.cuda.device_count():
        CUDA_SUPPORT = True
    else:
        CUDA_SUPPORT = False
    args = parse_server_args(sys.argv)
    if not CUDA_SUPPORT:
        args.gpu = "cpu"

    Logger.setup_logging(args.loglevel, logfile=args.logfile)
    logger = Logger()

    Logger.get().info(f"Server - CLI args: {args}")

    main(args)
