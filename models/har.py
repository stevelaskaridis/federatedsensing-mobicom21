import torch
from torch import nn


class LSTMClassifier(nn.Module):
    """Very simple implementation of LSTM-based time-series classifier."""

    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim):
        """
        Class ctor

        :param input_dim: Input dims
        :param hidden_dim: Hidden layer dims
        :param layer_dim: Layer dims
        :param output_dim: Output layer dims
        """
        super(LSTMClassifier, self).__init__()
        self.hidden_dim = hidden_dim
        self.layer_dim = layer_dim
        self.rnn = nn.LSTM(input_dim, hidden_dim, layer_dim, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)
        self.batch_size = None
        self.hidden = None

    def forward(self, x, hidden):
        """
        Forward propagation of model

        :param x: input data
        :param hidden: hidden input
        :return: output and hidden output
        """
        out, hidden_1 = self.rnn(x, hidden)
        out = self.fc(out[:, -1, :])
        return out, hidden_1

    def init_hidden(self, batch_size, device):
        """
        Initialise the hidden layer.

        :param batch_size: effective batch size
        :param device: Device model resides on.
        :return: The initialised hidden layer.
        """
        h0 = torch.zeros(
            self.layer_dim, batch_size, self.hidden_dim).to(device)
        c0 = torch.zeros(
            self.layer_dim, batch_size, self.hidden_dim).to(device)
        return [t for t in (h0, c0)]


def har_classifier(input_dim=3, hidden_dim=32, layer_dim=3,
                   num_classes=7):
    """
    Create our HAR classifier.

    :param input_dim: Input dims, defaults to 3
    :param hidden_dim: Hidden layer dims, defaults to 32
    :param layer_dim: Layer dims, defaults to 3
    :param num_classes: Output dims, defaults to 7
    :param pretrained: Whether to load pretrained model or not, defaults to False
    :return: The classifier
    """
    return LSTMClassifier(input_dim, hidden_dim, layer_dim, num_classes)