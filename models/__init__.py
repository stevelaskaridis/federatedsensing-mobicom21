# ResNet CIFAR implementations
from .resnet_cifar import resnet20
from .resnet_cifar import resnet32
from .resnet_cifar import resnet44
from .resnet_cifar import resnet56
from .resnet_cifar import resnet110
from .resnet_cifar import resnet1202

from .har import har_classifier as har

RNN_MODELS = ['LSTMClassifier']
