import flwr as fl
import os
import sys
import torch

import multiprocessing as mp
from multiprocessing import Process, Pipe
from utils.logger import Logger
from opts import parse_client_args
from utils.client_funcs import get_parameters, fit, evaluate_m


PIPE_TIMEOUT = 60
PIPE_POLL_ATTEMPTS = 10
TIMEOUT_EXIT_CODE = 1


class FLClient(fl.client.Client):
    """
    Flower client class
    """
    def __init__(self, args):
        """
        The ctor

        :param args: The ctor args
        """
        self.args = args

    def _launch(self, name, fun, args):
        """
        Launch function as a process.
        :param name: Name the function to be invoked
        :param fun: Functor to invoke
        :param args: Arguments to the functor
        """
        parent_conn, child_conn = Pipe()
        args = list(args)
        args.append(child_conn)
        Logger.get().info(f"Starting process for fun {name}")
        p = Process(target=fun, args=args)
        p.start()
        Logger.get().info(f"Waiting for result of fun {name}")
        attempts = 0
        while not parent_conn.poll(PIPE_TIMEOUT) and \
                attempts < PIPE_POLL_ATTEMPTS:
            attempts += 1
        else:
            if attempts >= PIPE_POLL_ATTEMPTS:
                Logger.get().warning("The maximum client timeout "
                                     f"({PIPE_POLL_ATTEMPTS}x{PIPE_TIMEOUT}="
                                     f"{PIPE_POLL_ATTEMPTS*PIPE_TIMEOUT}s)"
                                     "has been reached.")
                Logger.get().warning(f"Killing child subprocess ({p.pid})")
                p.kill()
                Logger.get().error(
                    f"Exiting client {args[0].cid} ({os.getpid()}) "
                    "with exit code {TIMEOUT_EXIT_CODE}")
                exit(TIMEOUT_EXIT_CODE)
            else:
                out = parent_conn.recv()
                Logger.get().info(f"Joining process of fun {name}")
                p.join()
                Logger.get().info(f"Returning result of {name}")
        return out

    def get_parameters(self):
        """
        Get parameters from client
        """
        weights: fl.common.Weights = self._launch(
            'get_parameters',
            get_parameters,
            (self.args,)
            )
        parameters = fl.common.weights_to_parameters(weights)

        return fl.common.ParametersRes(parameters=parameters)

    def fit(self, ins: fl.common.FitIns) -> fl.common.FitRes:
        """
        Client model training function

        :param ins: Training instructions (from server)
        :return: Fit result (to server)
        """
        weights, num_examples_train, fit_duration = self._launch(
            'fit',
            fit,
            (self.args, ins.config, ins.parameters.tensors)
        )

        # Return the refined weights and the #examples used for training
        weights_prime: fl.common.Weights = weights
        params_prime = fl.common.weights_to_parameters(weights_prime)

        return fl.common.FitRes(
            parameters=params_prime,
            num_examples=num_examples_train,
            num_examples_ceil=num_examples_train,
            fit_duration=fit_duration,
            metrics={
                'id': self.args.cid,
            }
        )

    def evaluate(self, ins: fl.common.EvaluateIns) -> fl.common.EvaluateRes:
        """
        Client model evaluation function

        :param ins: Evaluation instructions (from server)
        :return: Evaluation result (to server)
        """
        num_examples, loss, accuracy = self._launch(
            'evaluate',
            evaluate_m,
            (self.args, ins.config, ins.parameters.tensors)
            )

        return fl.common.EvaluateRes(
            num_examples=num_examples, loss=float(loss),
            accuracy=float(accuracy))


def main(args):
    args.device = args.gpu[0] if type(args.gpu) == list else args.gpu
    args.resume_from = None
    args.load_best = False

    # Create the client object
    client = FLClient(args)

    fl.client.start_client(args.server_address, client)


if __name__ == "__main__":
    global CUDA_SUPPORT
    if torch.cuda.device_count():
        CUDA_SUPPORT = True
    else:
        CUDA_SUPPORT = False

    mp.set_start_method('spawn')

    args = parse_client_args(sys.argv)

    if not CUDA_SUPPORT:
        args.gpu = "cpu"

    Logger.setup_logging(args.loglevel, logfile=args.logfile)
    logger = Logger()

    Logger.get().info(f"Client #{args.cid} -  CLI args: {args}")

    main(args)
