import argparse
from datetime import datetime
import os
import time


def parse_server_args(args, call_common=True, parser=None):
    """
    Parse arguments for server side functionality.
    :param args: The argument to pass.
    :param call_common: Whether to call common args in the end or not.
    :param parser: Parser object to inherit (None will initialise)
    :return: The parsed args
    """
    if parser is None:
        parser = initialise_arg_parser(args, 'FL server implementation.')

    parser.add_argument(
        "--rounds",
        type=int,
        default=1,
        help="Number of rounds of federated learning",
    )
    parser.add_argument(
        "--sample-fraction",
        type=float,
        default=1.0,
        help="Fraction of available clients used for fit/evaluate",
    )
    parser.add_argument(
        "--min-sample-size",
        type=int,
        default=2,
        help="Minimum number of clients used for fit/evaluate",
    )
    parser.add_argument(
        "--min-num-clients",
        type=int,
        default=2,
        help="Minimum number of available clients required for sampling",
    )
    parser.add_argument(
        "--client-eval",
        action='store_true',
        default=False,
        help="Evaluate on server or on samples of clients"
    )
    parser.add_argument(
        "--checkpoint-dir",
        type=str,
        default='../checkpoints',
        help="Directory to persist best/last models."
    )
    parser.add_argument(
        "--run-setup",
        type=str,
        default='server',
        help="Key for checkpoint dirs"
    )
    parser.add_argument(
        "--resume-from",
        type=str,
        help="Path to load pretrained model from"
    )

    if call_common:
        parse_common_args(args, parser)

    args = parser.parse_args()
    transform_gpu_args(args)

    return args


def parse_client_args(args, call_common=True, parser=None):
    """
    Parse arguments for client side functionality.
    :param args: The argument to pass.
    :param call_common: Whether to call common args in the end or not.
    :param parser: Parser object to inherit (None will initialise)
    :return: The parsed args
    """
    if parser is None:
        parser = initialise_arg_parser(args, 'FL server implementation.')

    parser.add_argument(
        "--cid",
        type=int,
        default=-1,
        help="Client CID (no default)"
    )
    parser.add_argument(
        "--lda-alpha",
        type=float,
        default=0.1,
        help="LDA concentration to load CIFAR-10 data from."
    )
    parser.add_argument(
        "--nb-clients",
        type=int,
        default=10,
        help="Total number of clients",
    )
    parser.add_argument(
        '--lr',
        type=float,
        default=0.1,
        help='initial learning rate (default: 0.1)'
    )
    parser.add_argument(
        '--lr-type',
        type=str,
        choices=['cosine', 'cifar_1', 'cifar_2', 'static'],
        default='static',
        help='Learning rate strategy (default: static)'
    )
    parser.add_argument(
        "--optimiser",
        type=str,
        choices=['sgd', 'adam', 'rmsprop'],
        default='sgd',
        help='Optimiser to use'
    )
    parser.add_argument(
        '--momentum',
        type=float,
        default=0.9,
        help='Momentum (default: 0.9)'
    )
    parser.add_argument(
        "--nesterov",
        type=bool,
        default=False,
        help='Nesterov momentum is used for momentum'
    )
    parser.add_argument(
        '--weight-decay',
        type=float,
        default=1e-4,
        help='weight decay (default: 1e-4)'
    )
    parser.add_argument(
        "--run-setup",
        type=str,
        default='client',
        help="Key for checkpoint dirs"
    )

    if call_common:
        parse_common_args(args, parser)

    args = parser.parse_args()
    transform_gpu_args(args)

    return args


def parse_common_args(args, parser):
    """
    Parse arguments for common functionality between server and client.
    :param args: The argument to parse.
    :param parser: Parser object to inherit
    :return: The parsed args
    """
    parser.add_argument(
        "--server-address",
        type=str,
        default="127.0.0.1:8081",
        help="gRPC server address",
    )
    parser.add_argument(
        "--dataset",
        type=str,
        required=True,
        choices=[
            "har", "cifar10"],
        help="Define which dataset to load"
    )
    parser.add_argument(
        "--data-path",
        type=str,
        default="../data/",
        help="Base root directory for the dataset."
    )
    parser.add_argument(
        "--model",
        type=str,
        help="Define which model to load"
    )
    parser.add_argument(
        "--gpu",
        type=str,
        default="0",
        help="Define on which GPU to run the model (comma-separated for many)."
        "If -1, use CPU."
    )
    parser.add_argument(
        "-e", "--epochs",
        type=int,
        default=None,
        help="Static number of epochs to run training for defined by "
        "the server training configuration"
    )
    parser.add_argument(
        "-b", "--batch-size",
        type=int,
        default=32,
        help="Static batch size defined by the server training configuration"
    )
    parser.add_argument(
        "-n", "--num-workers",
        type=int,
        default=1,
        help="Num workers for dataset loading"
    )
    parser.add_argument(
        "--deterministic",
        action="store_true",
        default=False,
        help="Run deterministically for reproducibility."
    )
    parser.add_argument(
        "--manual-seed",
        type=int,
        default=123,
        help="Random seed to use."
    )
    parser.add_argument(
        "--eval-every",
        type=int,
        default=10,
        help="How often to do validation."
    )
    parser.add_argument(
        "--run-id",
        type=str,
        default=str(time.time()),
        help="Identifier for the current job"
    )
    parser.add_argument(
        "--tb-dir",
        type=str,
        default="../runs",
        help="Directory to persist tensorboard logs"
    )
    parser.add_argument(
        "--loglevel",
        type=str,
        choices=["DEBUG", "INFO", "WARN", "ERROR", "CRITICAL"],
        default="INFO"
    )
    now = datetime.now()
    now = now.strftime("%Y%m%d%H%M%S")
    os.makedirs("../logs/", exist_ok=True)
    parser.add_argument(
        "--logfile",
        type=str,
        default=f"../logs/log_{now}.txt"
    )

    return args


def initialise_arg_parser(args, description):
    """
    Initialise the argument parser
    """
    parser = argparse.ArgumentParser(args, description=description)

    return parser


def transform_gpu_args(args):
    """
    Transformations for GPU to CPU change.
    """
    if args.gpu == "-1":
        args.gpu = "cpu"
    else:
        gpu_str_arg = args.gpu.split(',')
        if len(gpu_str_arg) > 1:
            args.gpu = sorted([int(card) for card in gpu_str_arg])
        else:
            args.gpu = f"cuda:{args.gpu}"
