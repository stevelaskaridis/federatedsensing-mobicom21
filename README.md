# Federated Sensing

This repository contains code for a Mobicom 2021 tutorial focused on learning from sensor data in a federated manner.

The dataset we are using is [MotionSense HAR](https://github.com/mmalekzadeh/motion-sense) and the codebase is based on [flwr](https://flower.dev/).

## Install requirements

```bash
conda create python=3.7 -n mobicom_tutorial
conda activate mobicom_tutorial
pip install -r requirements.txt
```

## How to run

```bash

cd scripts/
./preprocess_har.sh  # download and preprocess the HAR dataset
./har_experiment.sh  # run an Federated training job
```

We additionally provide an LDA-based variant of CIFAR-10 with Resnet-20. You can run it as follows:

```bash
cd scripts/
./preprocess_cifar10.sh  # download and preprocess the HAR dataset
./cifar10_experiment.sh  # run an Federated training job
```

## Citation

If you bootstrap with this repository, please cite us here:

```
@inproceedings{10.1145/3447993.3488031,
author = {Laskaridis, Stefanos and Spathis, Dimitris and Almeida, Mario},
title = {Federated Mobile Sensing for Activity Recognition},
year = {2021},
isbn = {9781450383424},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3447993.3488031},
doi = {10.1145/3447993.3488031},
abstract = {Despite advances in hardware and software enabling faster on-device inference, training Deep Neural Networks (DNN) models has largely been a long-running task over TBs of collected user data in centralised repositories. Federated Learning has emerged as an alternative, privacy-preserving paradigm to train models without accessing directly on-device data, by leveraging device resources to create per client updates and aggregate centrally. This has been applied to various tasks, ranging from next-word prediction to automatic speech recognition (ASR). In this tutorial, we recognise on-device sensing as a privacy-sensitive task and build a federated learning system from scratch to showcase how to train a model for accelerometer-based activity recognition in a federated manner. In addition, we present the current landscape and challenges in the realm of federated learning and mobile sensing and provide guidelines on how to build such systems in a privacy-preserving and scalable manner.},
booktitle = {Proceedings of the 27th Annual International Conference on Mobile Computing and Networking},
pages = {858–859},
numpages = {2},
location = {New Orleans, Louisiana},
series = {MobiCom '21}
}
```
